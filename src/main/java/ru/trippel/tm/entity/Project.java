package ru.trippel.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.entity.ISortable;
import ru.trippel.tm.enumeration.Status;
import ru.trippel.tm.util.DateUtil;
import java.util.Date;
import java.util.Objects;

@Setter
@Getter
@NoArgsConstructor
public final class Project extends AbstractEntity implements ISortable {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Date dateStart = new Date();

    @NotNull
    private Date dateFinish = new Date();

    @NotNull
    private String userId = "";

    @NotNull
    private Status status = Status.PLANNED;

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        @NotNull final Project project = (Project) o;
        return Objects.equals(id, project.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @NotNull
    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                ", UserId=" + userId +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", dateStart=" + DateUtil.dateFormat(dateStart) +
                ", dateFinish=" + DateUtil.dateFormat(dateFinish) +
                ", status=" + status.getDisplayName() +
                '}';
    }

}