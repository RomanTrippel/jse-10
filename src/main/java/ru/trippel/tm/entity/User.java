package ru.trippel.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.enumeration.SortingMethod;
import ru.trippel.tm.enumeration.TypeRole;
import java.util.Objects;

@Setter
@Getter
@NoArgsConstructor
public final class User extends AbstractEntity {

    @NotNull
    private String loginName = "";

    @NotNull
    private String password = "";

    @NotNull
    private TypeRole role = TypeRole.USER;

    @NotNull
    private SortingMethod projectSortingMethod = SortingMethod.CREATION_ORDER;

    @NotNull
    private SortingMethod taskSortingMethod = SortingMethod.CREATION_ORDER;

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        @NotNull final User user = (User) o;
        return Objects.equals(loginName, user.loginName) &&
                Objects.equals(password, user.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(loginName);
    }

    @NotNull
    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + loginName + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                ", projectSortingMethod=" + projectSortingMethod +
                ", taskSortingMethod=" + taskSortingMethod +
                '}';
    }
    
}
