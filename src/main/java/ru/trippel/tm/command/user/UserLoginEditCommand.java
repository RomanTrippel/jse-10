package ru.trippel.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.User;
import java.io.IOException;

@NoArgsConstructor
public final class UserLoginEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "user login edit";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit a profile.";
    }

    @Override
    public void execute() throws IOException {
        @Nullable final User currentUser = serviceLocator.getStateService().getCurrentUser();
        @NotNull final String userId = currentUser.getId();
        @Nullable final User user = serviceLocator.getUserService().findOne(userId);
        if (user == null) {
            System.out.println("Editable user is not defined.");
            return;
        }
        System.out.println("Enter new Login.");
        @NotNull final String newLogin = serviceLocator.getTerminalService().read();
        user.setLoginName(newLogin);
        serviceLocator.getUserService().merge(user);
        serviceLocator.getStateService().setCurrentUser(null);
        System.out.println("Changes applied. Login required again.");
    }

}
