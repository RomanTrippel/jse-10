package ru.trippel.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.util.HashPasswordUtil;

@NoArgsConstructor
public final class UserCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "user create";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create a user, you must enter a username and password.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final User user = new User();
        System.out.println("Enter login name.");
        @NotNull final String name = serviceLocator.getTerminalService().read();
        if (serviceLocator.getUserService().checkLogin(name)) {
            System.out.println("This name already exists, try again.");
            return;
        }
        user.setLoginName(name);
        System.out.println("Enter password.");
        @NotNull final String password = serviceLocator.getTerminalService().read();
        @NotNull final String passwordHash = HashPasswordUtil.getHash(password);
        user.setPassword(passwordHash);
        serviceLocator.getUserService().persist(user);
        System.out.println("User \"" + user.getLoginName() + "\" added!");
    }

}
