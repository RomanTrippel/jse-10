package ru.trippel.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.enumeration.TypeRole;
import java.util.List;

@NoArgsConstructor
public final class UserViewCommand extends AbstractCommand {

    {
        setRole(TypeRole.ADMIN);
    }

    @NotNull
    @Override
    public String getNameCommand() {
        return "user view";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "View all user names.";
    }

    @Override
    public void execute() {
        @NotNull final List<User> userList = serviceLocator.getUserService().findAll();
        if (userList.size() == 0) return;
        for (@NotNull final User user : userList) {
            System.out.println(user);
        }
    }

}
