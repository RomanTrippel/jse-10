package ru.trippel.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.util.HashPasswordUtil;

@NoArgsConstructor
public final class UserPassChangeCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "user pass change";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change Password.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User currentUser = serviceLocator.getStateService().getCurrentUser();
        @NotNull final String userId = currentUser.getId();
        @Nullable final User user = serviceLocator.getUserService().findOne(userId);
        if (user == null) return;
        System.out.println("Enter new Password");
        @NotNull final String newPassword = serviceLocator.getTerminalService().read();
        @NotNull final String newPasswordHash = HashPasswordUtil.getHash(newPassword);
        user.setPassword(newPasswordHash);
        serviceLocator.getStateService().setCurrentUser(null);
        System.out.println("Changes applied. Login required again.");
    }

}
