package ru.trippel.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.User;

@NoArgsConstructor
public final class UserViewProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "user view profile";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "View profile.";
    }

    @Override
    public void execute() {
        @Nullable final User currentUser = serviceLocator.getStateService().getCurrentUser();
        @Nullable final String id = currentUser.getId();
        @Nullable final User user = serviceLocator.getUserService().findOne(id);
        System.out.println(user);
    }

}
