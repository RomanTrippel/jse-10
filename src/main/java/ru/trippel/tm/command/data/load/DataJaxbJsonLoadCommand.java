package ru.trippel.tm.command.data.load;

import lombok.NoArgsConstructor;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.service.ISubjectArea;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.enumeration.TypeRole;
import ru.trippel.tm.service.SubjectAreaService;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.IOException;

@NoArgsConstructor
public final class DataJaxbJsonLoadCommand extends AbstractCommand {

    {
        setRole(TypeRole.ADMIN);
    }

    @NotNull
    @Override
    public String getNameCommand() {
        return "data jaxb json load";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Loading data as Json using JAXB.";
    }

    @Override
    public void execute() throws IOException, JAXBException {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final File file = new File("data/jaxb.json");
        @NotNull final StreamSource streamSource = new StreamSource(file);
        @NotNull final JAXBContext context = JAXBContext.newInstance(SubjectAreaService.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        @NotNull final ISubjectArea subjectAreaService =
                unmarshaller.unmarshal(streamSource, SubjectAreaService.class).getValue();
        serviceLocator.getStateService().setCurrentUser(null);
        subjectAreaService.write(serviceLocator);
        System.out.println("Data loaded.");
        System.out.println("You need to log in again.");
    }

}
