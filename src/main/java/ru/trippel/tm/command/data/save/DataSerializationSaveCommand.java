package ru.trippel.tm.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.service.ISubjectArea;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.enumeration.TypeRole;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

@NoArgsConstructor
public final class DataSerializationSaveCommand extends AbstractCommand {

    {
        setRole(TypeRole.ADMIN);
    }

    @NotNull
    @Override
    public String getNameCommand() {
        return "data ser save";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Saving data.";
    }

    @Override
    public void execute() throws IOException {
        @NotNull final ISubjectArea subjectAreaService = serviceLocator.getSubjectAreaService();
        subjectAreaService.read(serviceLocator);
        @NotNull final File folder = new File("data");
        if (!folder.exists()) folder.mkdir();
        @NotNull final FileOutputStream fos = new FileOutputStream("data/data.bin");
        @NotNull final ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(subjectAreaService);
        oos.close();
        fos.close();
        System.out.println("Data saved.");
    }

}
