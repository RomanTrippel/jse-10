package ru.trippel.tm.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.service.ISubjectArea;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.enumeration.TypeRole;
import ru.trippel.tm.service.SubjectAreaService;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;

@NoArgsConstructor
public final class DataJaxbXmlSaveCommand extends AbstractCommand {

    {
        setRole(TypeRole.ADMIN);
    }

    @NotNull
    @Override
    public String getNameCommand() {
        return "data jaxb xml save";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Saving data as XML using JAXB.";
    }

    @Override
    public void execute() throws JAXBException {
        @NotNull final ISubjectArea subjectAreaService = new SubjectAreaService();
        subjectAreaService.read(serviceLocator);
        @NotNull final File folder = new File("data");
        if (!folder.exists()) folder.mkdir();
        @NotNull final File file = new File("data/jaxb.xml");
        @NotNull final JAXBContext context = JAXBContext.newInstance(SubjectAreaService.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(subjectAreaService,file);
        System.out.println("Data saved.");
    }

}
