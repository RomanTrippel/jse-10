package ru.trippel.tm.command.data.save;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.service.ISubjectArea;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.enumeration.TypeRole;
import ru.trippel.tm.service.SubjectAreaService;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@NoArgsConstructor
public final class DataFasterxmlXmlSaveCommand extends AbstractCommand {

    {
        setRole(TypeRole.ADMIN);
    }

    @NotNull
    @Override
    public String getNameCommand() {
        return "data fasterxml xml save";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Saving data as XML using FasterXML.";
    }

    @Override
    public void execute() throws IOException {
        @NotNull final ISubjectArea subjectAreaService = new SubjectAreaService();
        subjectAreaService.read(serviceLocator);
        @NotNull final File folder = new File("data");
        if (!folder.exists()) folder.mkdir();
        @NotNull final File file = new File("data/fasterxml.xml");
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm a z");
        xmlMapper.setDateFormat(df);
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(file, subjectAreaService);
        System.out.println("Data saved.");
    }

}
