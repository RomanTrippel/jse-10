package ru.trippel.tm.command.data.load;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.service.ISubjectArea;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.enumeration.TypeRole;
import ru.trippel.tm.service.SubjectAreaService;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;

@NoArgsConstructor
public final class DataJaxbXmlLoadCommand extends AbstractCommand {

    {
        setRole(TypeRole.ADMIN);
    }

    @NotNull
    @Override
    public String getNameCommand() {
        return "data jaxb xml load";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Loading data as XML using JAXB.";
    }

    @Override
    public void execute() throws IOException, JAXBException {
        @NotNull final File file = new File("data/jaxb.xml");
        @NotNull final JAXBContext context = JAXBContext.newInstance(SubjectAreaService.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final ISubjectArea subjectAreaService = (ISubjectArea) unmarshaller.unmarshal(file);
        serviceLocator.getStateService().setCurrentUser(null);
        subjectAreaService.write(serviceLocator);
        System.out.println("Data loaded.");
        System.out.println("You need to log in again.");
    }

}
