package ru.trippel.tm.command.data.load;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.service.ISubjectArea;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.enumeration.TypeRole;
import ru.trippel.tm.service.SubjectAreaService;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@NoArgsConstructor
public final class DataFasterxmlJsonLoadCommand extends AbstractCommand {

    {
        setRole(TypeRole.ADMIN);
    }

    @NotNull
    @Override
    public String getNameCommand() {
        return "data fasterxml json load";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Loading data as Json using FasterXML.";
    }

    @Override
    public void execute() throws IOException {
        @NotNull final File file = new File("data/fasterxml.json");
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        @NotNull final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm a z");
        objectMapper.setDateFormat(df);
        @NotNull final ISubjectArea subjectAreaService = objectMapper.readValue(file, SubjectAreaService.class);
        serviceLocator.getStateService().setCurrentUser(null);
        subjectAreaService.write(serviceLocator);
        System.out.println("Data loaded.");
        System.out.println("You need to log in again.");
    }

}
