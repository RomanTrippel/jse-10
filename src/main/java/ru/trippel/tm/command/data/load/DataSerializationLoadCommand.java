package ru.trippel.tm.command.data.load;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.service.ISubjectArea;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.enumeration.TypeRole;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

@NoArgsConstructor
public final class DataSerializationLoadCommand extends AbstractCommand {

    {
        setRole(TypeRole.ADMIN);
    }

    @NotNull
    @Override
    public String getNameCommand() {
        return "data ser load";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Loading data.";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        @NotNull final FileInputStream fis = new FileInputStream("data/data.bin");
        @NotNull final ObjectInputStream ois = new ObjectInputStream(fis);
        @NotNull final ISubjectArea subjectAreaService = (ISubjectArea) ois.readObject();
        serviceLocator.getStateService().setCurrentUser(null);
        subjectAreaService.write(serviceLocator);
        ois.close();
        fis.close();
        System.out.println("Data loaded.");
        System.out.println("You need to log in again.");
    }

}
