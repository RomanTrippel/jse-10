package ru.trippel.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.enumeration.SortingMethod;
import java.util.List;

@NoArgsConstructor
public final class ProjectViewCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "project view";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        @Nullable final User currentUser = serviceLocator.getStateService().getCurrentUser();
        @NotNull final String userId = currentUser.getId();
        @NotNull final SortingMethod sortingMethod = currentUser.getProjectSortingMethod();
        @Nullable final List<Project> projectList = serviceLocator.getProjectService().findAll(userId, sortingMethod);
        if (projectList == null || projectList.isEmpty()) {
            System.out.println("List is empty.");
            return;
        }
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println(i+1 + ". " + projectList.get(i));
        }
    }

}