package ru.trippel.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.entity.User;
import java.util.List;

@NoArgsConstructor
public final class ProjectAttachTaskCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "project attach task";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Attach a Task to a Project.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User currentUser = serviceLocator.getStateService().getCurrentUser();
        @NotNull final String userId = currentUser.getId();
        @Nullable final List<Project> projectList = serviceLocator.getProjectService().findAll(userId);
        if (projectList == null || projectList.isEmpty()) {
            System.out.println("List is empty.");
            return;
        }
        @Nullable final List<Task> taskList = serviceLocator.getTaskService().findAll(userId);
        if (taskList == null || taskList.isEmpty()) {
            System.out.println("List is empty.");
            return;
        }
        int projectNum = -1;
        int taskNum = -1;
        System.out.println("Projects List:");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println(i+1 + ". " + projectList.get(i).getName());
        }
        System.out.println("Enter a project number");
        projectNum +=  Integer.parseInt(serviceLocator.getTerminalService().read());
        @NotNull final String projectId = projectList.get(projectNum).getId();
        System.out.println("Tasks List:");
        for (int i = 0; i < taskList.size(); i++) {
            if (taskList.get(i).getProjectId().isEmpty()) {
                System.out.println(i+1 + ". " + taskList.get(i).getName());
            }
        }
        System.out.println("Enter a task number");
        taskNum +=  Integer.parseInt(serviceLocator.getTerminalService().read());
        @NotNull final String taskId = taskList.get(taskNum).getId();
        @Nullable final Task task = serviceLocator.getTaskService().findOne(taskId);
        if (task == null) return;
        task.setProjectId(projectId);
        System.out.println("The task attached.");
    }

}
