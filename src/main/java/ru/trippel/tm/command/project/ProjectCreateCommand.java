package ru.trippel.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.User;

@NoArgsConstructor
public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "project create";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create a new project.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Project project = new Project();
        @Nullable final User currentUser = serviceLocator.getStateService().getCurrentUser();
        @NotNull final String userId = currentUser.getId();
        System.out.println("Enter project name.");
        @NotNull final String name = serviceLocator.getTerminalService().read();
        project.setName(name);
        project.setUserId(userId);
        serviceLocator.getProjectService().persist(project);
        System.out.println("The project \"" + project.getName() + "\" added!");
    }

}
