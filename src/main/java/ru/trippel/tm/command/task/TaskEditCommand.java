package ru.trippel.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.util.DateUtil;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
public final class TaskEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "task edit";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit a task.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User currentUser = serviceLocator.getStateService().getCurrentUser();
        @NotNull final String userId = currentUser.getId();
        @Nullable final List<Task> taskList = serviceLocator.getTaskService().findAll(userId);
        if (taskList == null || taskList.isEmpty()) {
            System.out.println("List is empty.");
            return;
        }
        int taskNum = -1;
        System.out.println("Task List:");
        for (int i = 0; i < taskList.size(); i++) {
            System.out.println(i + 1 + ". " + taskList.get(i).getName());
        }
        System.out.println("Enter a task number to Edit.");
        taskNum +=  Integer.parseInt(serviceLocator.getTerminalService().read());
        @NotNull final String taskId = taskList.get(taskNum).getId();
        @Nullable final Task task = serviceLocator.getTaskService().findOne(taskId);
        if (task == null) return;
        System.out.println("If you don't want to change, leave the line empty.");
        System.out.println("Enter new Name.");
        @NotNull final String newName = serviceLocator.getTerminalService().read();
        if (!newName.isEmpty()) task.setName(newName);
        System.out.println("Enter description.");
        @NotNull final String newDescription = serviceLocator.getTerminalService().read();
        if (!newDescription.isEmpty()) task.setDescription(newDescription);
        System.out.println("Enter the start date of the task.(For example, 13-01-2020)");
        @NotNull final String newDateStart = serviceLocator.getTerminalService().read();
        if (!newDateStart.isEmpty()) {
            @NotNull final Date dateStart = DateUtil.parseDate(newDateStart);
            task.setDateStart(dateStart);
        }
        System.out.println("Enter the finish date of the task.(For example, 13-01-2020)");
        @NotNull final String newDateFinish = serviceLocator.getTerminalService().read();
        if (!newDateFinish.isEmpty()) {
            @NotNull final Date dateFinish = DateUtil.parseDate(newDateFinish);
            task.setDateFinish(dateFinish);
        }
        serviceLocator.getTaskService().merge(task);
        System.out.println("Changes applied.");
    }

}
