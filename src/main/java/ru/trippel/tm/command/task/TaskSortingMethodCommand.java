package ru.trippel.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.enumeration.SortingMethod;

@NoArgsConstructor
public final class TaskSortingMethodCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "task sort method";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change the sorting method.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User currentUser = serviceLocator.getStateService().getCurrentUser();
        @NotNull final SortingMethod[] sortingMethod = SortingMethod.values();
        for (int i = 0; i < sortingMethod.length; i++) {
            System.out.println(i+1 + ". " +sortingMethod[i].getDisplayName());
        }
        System.out.println("Enter a sorting method.");
        int statusNum = -1;
        statusNum +=  Integer.parseInt(serviceLocator.getTerminalService().read());
        @NotNull final SortingMethod newSortingMethod = sortingMethod[statusNum];
        currentUser.setTaskSortingMethod(newSortingMethod);
        System.out.println("Changes applied.");
    }

}
