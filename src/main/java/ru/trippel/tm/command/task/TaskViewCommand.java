package ru.trippel.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.enumeration.SortingMethod;
import java.util.List;

@NoArgsConstructor
public final class TaskViewCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "task view";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        @Nullable final User currentUser = serviceLocator.getStateService().getCurrentUser();
        @NotNull final String userId = currentUser.getId();
        @NotNull final SortingMethod sortingMethod = currentUser.getTaskSortingMethod();
        @Nullable final List<Task> taskList = serviceLocator.getTaskService().findAll(userId, sortingMethod);
        if (taskList == null || taskList.isEmpty()) {
            System.out.println("List is empty.");
            return;
        }
        System.out.println("Task List:");
        for (int i = 0; i < taskList.size(); i++) {
            System.out.println(i+1 + ". " + taskList.get(i));
        }
    }

}
