package ru.trippel.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.entity.User;

@NoArgsConstructor
public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "task create";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create a new task.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Task task = new Task();
        @Nullable final User currentUser = serviceLocator.getStateService().getCurrentUser();
        @NotNull final String userId = currentUser.getId();
        System.out.println("Enter task name.");
        @NotNull final String name = serviceLocator.getTerminalService().read();
        task.setName(name);
        task.setUserId(userId);
        serviceLocator.getTaskService().persist(task);
        System.out.println("The task \"" + task.getName() + "\" added!");
    }

}
