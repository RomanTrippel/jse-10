package ru.trippel.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.enumeration.Status;

import java.util.List;

@NoArgsConstructor
public final class TaskSetStatusCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "task set status";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change of task status.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User currentUser = serviceLocator.getStateService().getCurrentUser();
        @NotNull final String userId = currentUser.getId();
        @Nullable final List<Task> taskList = serviceLocator.getTaskService().findAll(userId);
        if (taskList == null || taskList.isEmpty()) {
            System.out.println("List is empty.");
            return;
        }
        int taskNum = -1;
        System.out.println("Task List:");
        for (int i = 0; i < taskList.size(); i++) {
            System.out.println(i+1 + ". " + taskList.get(i).getName());
        }
        System.out.println("Enter a task number to change status.");
        taskNum +=  Integer.parseInt(serviceLocator.getTerminalService().read());
        @NotNull final String taskId = taskList.get(taskNum).getId();
        @Nullable final Task task = serviceLocator.getTaskService().findOne(taskId);
        if (task == null) return;
        @NotNull final Status[] status = Status.values();
        for (int i = 0; i < status.length; i++) {
            System.out.println(i+1 + ". " +status[i].getDisplayName());
        }
        System.out.println("Enter a status.");
        int statusNum = -1;
        statusNum +=  Integer.parseInt(serviceLocator.getTerminalService().read());
        @NotNull final Status newStatus = status[statusNum];
        task.setStatus(newStatus);
        System.out.println("Changes applied.");
    }

}
