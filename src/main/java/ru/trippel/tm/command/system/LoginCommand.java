package ru.trippel.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.util.HashPasswordUtil;

import java.util.List;

@NoArgsConstructor
public final class LoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "login";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Authorization, you must enter a username and password.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final List<User> userList = serviceLocator.getUserService().findAll();
        @NotNull final User userTemp = new User();
        System.out.println("Enter login:");
        @NotNull final String login = serviceLocator.getTerminalService().read();
        userTemp.setLoginName(login);
        System.out.println("Enter password:");
        @NotNull final String password = serviceLocator.getTerminalService().read();
        @NotNull final String passwordHash = HashPasswordUtil.getHash(password);
        userTemp.setPassword(passwordHash);
        for (@NotNull final User user : userList) {
            if (user.equals(userTemp)) {
                serviceLocator.getStateService().setCurrentUser(user);
                System.out.println("You have successfully logged in.");
                return;
            }
        }
        System.out.println("You entered the login or password incorrectly.");
    }

}
