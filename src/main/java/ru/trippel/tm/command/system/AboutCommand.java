package ru.trippel.tm.command.system;

import com.jcabi.manifests.Manifests;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.command.AbstractCommand;

@NoArgsConstructor
public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "about";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Information about the application.";
    }

    @Override
    public void execute() {
        @NotNull final String buildNumber = Manifests.read("buildNum");
        System.out.println("Information about the application:\n" +
                "build number: " + "\n" + buildNumber) ;
    }

}