package ru.trippel.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.User;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    List<User> findAll();

    @Nullable
    User findOne(@NotNull String id);

    @Nullable
    User persist(@NotNull User user);

    @Nullable
    User merge(@NotNull User user);

    @Nullable
    User remove(@NotNull String id);

    void removeAll();

    void persist(@NotNull List<User> userList);
}
