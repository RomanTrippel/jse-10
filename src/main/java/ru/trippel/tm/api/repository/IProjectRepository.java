package ru.trippel.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Project;
import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @NotNull
    List<Project> findAll();

    @Nullable
    List<Project> findAll(@NotNull String userId);

    @Nullable
    List<Project> findAll(@NotNull String userId, @NotNull Comparator<Project> comparator);

    @Nullable
    Project findOne(@NotNull String id);

    @Nullable
    List<Project> findByPart(@NotNull String userId, @NotNull String searchText);

    @Nullable
    Project persist(@NotNull Project project);

    @Nullable
    Project merge(@NotNull Project project);

    @Nullable
    Project remove(@NotNull String id);

    void removeAll();

    void clear(@NotNull String userId);

    void persist(@NotNull List<Project> projectList);
}
