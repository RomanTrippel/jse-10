package ru.trippel.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Task;
import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository <Task> {

    @NotNull
    List<Task> findAll();

    @Nullable
    List<Task> findAll(@NotNull String userId);

    @Nullable
    List<Task> findAll(@NotNull String userId, @NotNull Comparator<Task> comparator);

    @Nullable
    Task findOne(@NotNull String id);

    @Nullable
    List<Task> findByPart(@NotNull String userId, @NotNull String searchText);

    @Nullable
    Task persist(@NotNull Task task);

    @Nullable
    Task merge(@NotNull Task task);

    @Nullable
    Task remove(@NotNull String id);

    void removeAll();

    void clear(@NotNull String userId);

    void removeByProjectId(@NotNull String projectId);

    void persist(@NotNull List<Task> taskList);
}
