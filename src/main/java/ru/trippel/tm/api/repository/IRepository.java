package ru.trippel.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IRepository<T> {

    @NotNull
    List<T> findAll();

    @Nullable
    List<T> findAll(@NotNull String userId);

    @Nullable
    T findOne(@NotNull String id);

    @Nullable
    T persist(@NotNull T t);

    @Nullable
    T merge(@NotNull T t);

    @Nullable
    T remove(@NotNull String id);

    void removeAll();

}
