package ru.trippel.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.User;
import java.util.List;

public interface IUserService extends IService<User> {

    @NotNull
    List<User> findAll();

    @Nullable
    User findOne(@Nullable String id);

    @Nullable
    User persist(@Nullable User user);

    @Nullable
    User merge(@Nullable User user);

    @Nullable
    User remove(@Nullable String id);

    boolean checkLogin(@Nullable final String name);

    void persist(@NotNull List<User> userList);
}
