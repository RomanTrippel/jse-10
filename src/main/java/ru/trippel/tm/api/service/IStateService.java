package ru.trippel.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.User;

import java.util.List;

public interface IStateService {

    @NotNull
    User getCurrentUser();

    void setCurrentUser(@Nullable User user);

    @NotNull
    List<AbstractCommand> getCommands();


}
