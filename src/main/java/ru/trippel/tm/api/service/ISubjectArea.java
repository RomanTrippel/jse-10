package ru.trippel.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.context.IServiceLocator;

public interface ISubjectArea {

    void write(@NotNull IServiceLocator serviceLocator);

    void read(@NotNull IServiceLocator serviceLocator);

}
