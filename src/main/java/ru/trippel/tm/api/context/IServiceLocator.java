package ru.trippel.tm.api.context;

import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.service.*;

public interface IServiceLocator {

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    ITerminalService getTerminalService();

    @NotNull
    ISubjectArea getSubjectAreaService();

    @NotNull
    IStateService getStateService();

}
