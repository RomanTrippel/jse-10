package ru.trippel.tm.view;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class BootstrapView {

    public static void printWelcome(){
        System.out.println("~~~~~Welcome to task manager~~~~~" +
                "\nEnter a command. For help and viewing the available commands, type \"help\".");
    }

    public static void printGoodbye(){
        System.out.println("~~~~~See you soon. Come again!~~~~~");
    }

    public static void printError() {
        System.out.println("Invalid request, try again.");
    }

    public static void printIncorrectlyData() {
        System.out.println("Data entered incorrectly.");
    }

    public static void printErrorCommand() {
        System.out.println("Invalid command.");
    }

    public static void printErrorValidation() {
        System.out.println("There are no permissions to execute this command.");
    }

}