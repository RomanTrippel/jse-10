package ru.trippel.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.repository.IProjectRepository;
import ru.trippel.tm.api.repository.ITaskRepository;
import ru.trippel.tm.api.repository.IUserRepository;
import ru.trippel.tm.api.service.*;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.enumeration.TypeRole;
import ru.trippel.tm.repository.ProjectRepository;
import ru.trippel.tm.repository.TaskRepository;
import ru.trippel.tm.repository.UserRepository;
import ru.trippel.tm.service.*;
import ru.trippel.tm.util.HashPasswordUtil;
import ru.trippel.tm.util.PackageClassFinderUtil;
import ru.trippel.tm.view.BootstrapView;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

@Getter
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final Map<String, AbstractCommand> commands = new TreeMap<>();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final ITerminalService terminalService = new TerminalService();

    @NotNull
    public final ISubjectArea subjectAreaService = new SubjectAreaService();

    @NotNull
    private final IServiceLocator serviceLocator = this;

    @NotNull
    private final IStateService stateService = new StateService(commands);

    public void start() throws Exception {
        BootstrapView.printWelcome();
        @NotNull String command = "";
        init();
        while (true) {
            try {
                command = terminalService.read();
                if ("EXIT".equals(command)) {
                    BootstrapView.printGoodbye();
                    break;
                }
                if (command.isEmpty()) {
                    BootstrapView.printErrorCommand();
                    continue;
                }
                execute(getCommand(command));
            }
            catch (NumberFormatException | ParseException e) {
                BootstrapView.printIncorrectlyData();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void execute(@Nullable final AbstractCommand command) throws Exception {
        if (command == null) return;
        @Nullable final AbstractCommand commandTMP = checkPermission(command);
        if (commandTMP == null) return;
            commandTMP.execute();
    }

    @Nullable
    private AbstractCommand getCommand(@Nullable final String command) {
        if (command == null) {
            return null;
        }
        if (command.isEmpty()) {
            return null;
        }
        @NotNull final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) BootstrapView.printErrorCommand();
        return abstractCommand;
    }

    @Nullable
    private AbstractCommand checkPermission(@Nullable final AbstractCommand command) {
        if (command == null) return null;
        if (command.secure()) return command;
        @Nullable final User currentUser = stateService.getCurrentUser();
        if (currentUser == null) {
            System.out.println("You need to login.");
            return null;
        }
        @NotNull final String loginName = currentUser.getLoginName();
        if (loginName.isEmpty()) {
            BootstrapView.printErrorValidation();
            return null;
        }
        if (TypeRole.ADMIN == currentUser.getRole()) return command;
        if (currentUser.getRole() == command.getRole()) return command;
        BootstrapView.printErrorValidation();
        return null;
    }

    public void init() throws NoSuchAlgorithmException, InstantiationException, IllegalAccessException {
        register();
        @NotNull final User admin = new User();
        admin.setLoginName("admin");
        admin.setPassword(HashPasswordUtil.getHash("admin"));
        admin.setRole(TypeRole.ADMIN);
        userService.persist(admin);
        @NotNull final User user = new User();
        user.setLoginName("user");
        user.setPassword(HashPasswordUtil.getHash("user"));
        userService.persist(user);
    }

    private void register() throws IllegalAccessException, InstantiationException {
        @NotNull AbstractCommand command;
        @NotNull final String pathCommandPackage = "ru.trippel.tm.command";
        @NotNull final Set<Class<? extends AbstractCommand>> allClasses = PackageClassFinderUtil.
                getAllClasses(pathCommandPackage);
        for (@NotNull final Class<? extends AbstractCommand> abstractCommand: allClasses) {
            command = abstractCommand.newInstance();
            command.setServiceLocator(serviceLocator);
            commands.put(command.getNameCommand(), command);
        }
    }

}
