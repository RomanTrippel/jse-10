package ru.trippel.tm.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.service.IStateService;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.User;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@RequiredArgsConstructor
public class StateService implements IStateService {

    @NotNull
    private final Map<String, AbstractCommand> commands;

    @Getter
    @Setter
    @Nullable
    private User currentUser = null;

    @NotNull
    @Override
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

}
