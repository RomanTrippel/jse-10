package ru.trippel.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.IUserRepository;
import ru.trippel.tm.api.service.IUserService;
import ru.trippel.tm.entity.User;

import java.util.List;

public final class UserService extends AbstractService <User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository repository) {
        super(repository);
        userRepository = repository;
    }

    @Override
    public boolean checkLogin(@Nullable final String name) {
        if (name == null) return false;
        if (name.isEmpty()) return false;
        @NotNull
        final List<User> userList = repository.findAll();
        for (@NotNull final User user: userList) {
            if (user.getLoginName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void persist(@NotNull final List<User> userList) {
        if (userList.size() == 0) return;
        userRepository.persist(userList);
    }

}
