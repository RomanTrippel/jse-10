package ru.trippel.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.IProjectRepository;
import ru.trippel.tm.api.service.IProjectService;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.enumeration.SortingMethod;
import ru.trippel.tm.util.ComparatorUtil;
import java.util.Comparator;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository repository) {
        super(repository);
        projectRepository = repository;
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null) return null;
        if (userId.isEmpty()) return null;
        return repository.findAll(userId);
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId, @NotNull final SortingMethod sortingMethod) {
        if (userId == null) return null;
        if (userId.isEmpty()) return null;
        @NotNull final Comparator<Project> comparator = ComparatorUtil.getComparator(sortingMethod);
        return projectRepository.findAll(userId, comparator);
    }

    @Nullable
    @Override
    public List<Project> findByPart(@Nullable final String userId, @NotNull final String searchText) {
        if (userId == null) return null;
        if (userId.isEmpty()) return null;
        return projectRepository.findByPart(userId, searchText);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null) return;
        if (userId.isEmpty()) return;
        projectRepository.clear(userId);
    }

    @Override
    public void persist(@NotNull final List<Project> projectList) {
        if (projectList.size() == 0) return;
        projectRepository.persist(projectList);
    }

}
