package ru.trippel.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.IRepository;
import ru.trippel.tm.api.service.IService;
import ru.trippel.tm.entity.AbstractEntity;
import java.util.List;

@RequiredArgsConstructor
public abstract class AbstractService <T extends AbstractEntity> implements IService<T> {

    @NotNull
    final protected IRepository<T> repository;

    @NotNull
    @Override
    public List<T> findAll() {
        return repository.findAll();
    }


    @Nullable
    @Override
    public T findOne(@Nullable final String id) {
        if (id == null) return null;
        if (id.isEmpty()) return null;
        return repository.findOne(id);
    }

    @Nullable
    @Override
    public T persist(@Nullable final T t) {
        if (t == null) return null;
        return repository.persist(t);
    }

    @Nullable
    @Override
    public T merge(@Nullable final T t) {
        if (t == null) return null;
        return repository.merge(t);
    }

    @Nullable
    @Override
    public T remove(@Nullable final String id) {
        if (id == null) return null;
        if (id.isEmpty()) return null;
        return repository.remove(id);
    }

    @Override
    public void removeAll() {
        repository.removeAll();
    }

}
