package ru.trippel.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.ITaskRepository;
import ru.trippel.tm.api.service.ITaskService;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.enumeration.SortingMethod;
import ru.trippel.tm.util.ComparatorUtil;
import java.util.Comparator;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    public TaskService(@NotNull final ITaskRepository repository) {
        super(repository);
        taskRepository = repository;
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null) return null;
        if (userId.isEmpty()) return null;
        return repository.findAll(userId);
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable final String userId, @NotNull final SortingMethod sortingMethod) {
        if (userId == null) return null;
        if (userId.isEmpty()) return null;
        @NotNull final Comparator<Task> comparator = ComparatorUtil.getComparator(sortingMethod);
        return taskRepository.findAll(userId, comparator);
    }

    @Nullable
    @Override
    public List<Task> findByPart(@Nullable final String userId, @NotNull final String searchText) {
        if (userId == null) return null;
        if (userId.isEmpty()) return null;
        return taskRepository.findByPart(userId, searchText);
    }

    @Override
    public void removeAllByProjectId(@NotNull final String projectId) {
        if (projectId.isEmpty()) return;
        taskRepository.removeByProjectId(projectId);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null) return;
        if (userId.isEmpty()) return;
        taskRepository.clear(userId);
    }

    @Override
    public void persist(@NotNull final List<Task> taskList) {
        if (taskList.size() == 0) return;
        taskRepository.persist(taskList);
    }

}
