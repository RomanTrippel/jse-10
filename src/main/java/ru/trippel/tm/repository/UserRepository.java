package ru.trippel.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.IUserRepository;
import ru.trippel.tm.entity.User;

import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    @Override
    public List<User> findAll(@NotNull final String userId) {
        @NotNull final List<User> listTemp = new LinkedList<>();
        @NotNull final List<User> listAll  = new LinkedList<>(map.values());
        for (@NotNull final User user: listAll) {
            if (user.getId().equals(userId)) listTemp.add(user);
        }
        return listTemp;
    }

    @Nullable
    @Override
    public User persist(@NotNull final User user) {
        @NotNull final String userId = user.getId();
        if (map.containsKey(userId)) return null;
        return map.put(userId,user);
    }

    @Override
    public void persist(@NotNull final List<User> userList) {
        map.clear();
        for (@NotNull final User user: userList) {
            if (map.containsKey(user.getId())) continue;
            if (map.containsValue(user)) continue;
            map.put(user.getId(),user);
        }
    }

}
