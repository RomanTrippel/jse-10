package ru.trippel.tm.util;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@NoArgsConstructor
public final class HashPasswordUtil {

    @NotNull
    public static String getHash(@NotNull final String pass) throws NoSuchAlgorithmException {
        @NotNull final String salt = "~какаятосоль*";
        @NotNull final String passWithSalt = String.format("%s %s %s", salt, pass, salt);
        @NotNull final MessageDigest digest = MessageDigest.getInstance("MD5");
        digest.reset();
        digest.update(passWithSalt.getBytes());
        @NotNull final BigInteger bigInt = new BigInteger(1, digest.digest());
        @NotNull final String result = bigInt.toString(16);
        return result;
    }

}
