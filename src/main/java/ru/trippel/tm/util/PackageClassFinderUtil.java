package ru.trippel.tm.util;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.trippel.tm.command.AbstractCommand;

import java.util.Set;

@NoArgsConstructor
public class PackageClassFinderUtil {

    @NotNull
    public static Set<Class<? extends AbstractCommand>> getAllClasses(@NotNull final String pathPackage) {
        @NotNull final Reflections reflections = new Reflections(pathPackage);
        @NotNull final Set<Class<? extends AbstractCommand>> allClasses =
                reflections.getSubTypesOf(AbstractCommand.class);
        return allClasses;
    }

}